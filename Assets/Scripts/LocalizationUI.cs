﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalizationUI : MonoBehaviour
{
    [SerializeField] private string key;

    private void Awake()
    {
        //key = GetComponent<TMPro.TMP_Text>().text;
        //LocalizationManager.Instance.Instance_OnChangeLanguages(this.gameObject, key);
    }


    //private void OnEnable()
    //{
    //    LocalizationManager.Instance.OnChangeLanguages += Instance_OnChangeLanguages;
    //}

    //private void OnDisable()
    //{
    //    LocalizationManager.Instance.OnChangeLanguages -= Instance_OnChangeLanguages;
    //}

    public string GetKey()
    {
        return key;
    }
    public void SetKey()
    {
        key = GetComponent<TMPro.TMP_Text>().text;
    }



    public void Instance_OnChangeLanguages()
    {
        gameObject.GetComponent<TMPro.TMP_Text>().text = LocalizationManager.Instance.GetWordFromDictionary(key);
    }
}
