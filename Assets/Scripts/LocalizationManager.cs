﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System;


public class LocalizationManager : Singleton<LocalizationManager>
{
    private static Dictionary<SystemLanguage,Dictionary<string, string>> allDictionary = new Dictionary<SystemLanguage, Dictionary<string, string>>();
    private static string missingTextString = "Localized text not found";
    private List<LocalizationUI> translatableElements;


    //populate the dictionary from the string which contains all the data
    private void PopulateDictionary(string dataAsJson)
    {
        //delete all the old values of the dictionary
        Dictionary<string, string> localizedText = new Dictionary<string, string>();

        //check if dataAsJson is null; if not populate the dictionary, else throw an error
        if (dataAsJson != null)
        {
            if (dataAsJson.Length > 0)
            {
                LocalizationData localizationData = JsonUtility.FromJson<LocalizationData>(dataAsJson);

                for (int i = 0; i < localizationData.items.Length; i++)
                {
                    localizedText.Add(localizationData.items[i].key, localizationData.items[i].value);
                }
                allDictionary.Add(localizationData.language, localizedText);
            }
            else
            {
                Debug.Log("Cannot populate the dictionary: dictionary is empty");
            }
        }
        else
        {
            Debug.Log("Cannot populate the dictionary: dictionary not found");
        }
    }

    //retrieve the json file and save its content in a string; the string will be used to populate the dictionary
    private IEnumerator LoadDictionary(string fileName)
    {
        //create the string in which save the file content
        string dataAsJson = null;

        //Get the path for the json file to upload in the dictionary
        string filePath = GetLocalizationFilePath(fileName);


#if UNITY_EDITOR || UNITY_STANDALONE
        //check if file exists and, if yes, read all the data and save it in dataAsJson
        if (File.Exists(filePath))
        {
            dataAsJson = File.ReadAllText(filePath);
        }
#elif UNITY_WEBGL
        Debug.Log(filePath);
        IEnumerator e = GameManager.Instance.GetRequest(filePath);

        while (e.MoveNext())
        {
            yield return null;
        }
        dataAsJson = (string)e.Current;
        Debug.Log(dataAsJson);
#endif
        //populate the dictionary with the string retrieved before and update the GUI
        PopulateDictionary(dataAsJson);
        yield break;
    }

    private string GetLocalizationFilePath(string fileName)
    {
        return Path.Combine(Application.streamingAssetsPath,"Localization","UI",fileName);
    }

    public IEnumerator LoadLanguages()
    {
        IEnumerable<string> files;
#if UNITY_EDITOR || UNITY_STANDALONE
        files = Directory.GetFiles(Path.Combine(Application.streamingAssetsPath, "Localization", "UI")).Where(o => !o.Contains(".meta"));
#elif UNITY_WEBGL
        IEnumerator e = GameManager.Instance.GetRequest(Path.Combine(Application.streamingAssetsPath, "Localization", "manifestUI"));

        while (e.MoveNext())
        {
            yield return null;
        }
        files = ((string)e.Current).Split(';');
#endif

        foreach (string file in files)
        {
            Debug.Log(file);
            StartCoroutine(LoadDictionary(file));
        }
        yield return null;
    }

    

    private void Awake()
    {
        StartCoroutine(LoadLanguages());
    }

    Dictionary<string, string> tempDictionary;
    string tempValue;
    public string GetWordFromDictionary(string key, SystemLanguage language = SystemLanguage.Unknown)
    {
        tempValue = missingTextString;
        if (allDictionary.TryGetValue(language==SystemLanguage.Unknown?GameManager.Instance.CurrentLanguageUI:language, out tempDictionary))
        {
            if(!tempDictionary.TryGetValue(key,out tempValue))
            {
                Debug.Log("ERRORE");
            }
        }
        return tempValue;
    }

    public Dictionary<int,string> GetAvailableLanguage()
    {
        Dictionary<int, string> languages = new Dictionary<int, string>();
        foreach(var dictionary in allDictionary)
        {
            languages.Add((int)dictionary.Key,System.Enum.GetName(typeof(SystemLanguage), dictionary.Key).Substring(0,3).ToUpper());
        }
        return languages;
    }

    public void LocalizeUI(SystemLanguage newLanguage)
    {
        GameManager.Instance.SetCurrentLanguageUI(newLanguage);
        Instance_OnChangeLanguages();
    }

    public void Instance_OnChangeLanguages()
    {
        foreach(var element in translatableElements)
        {
            element.Instance_OnChangeLanguages();
        }        
    }

    public void SetTranslatableUIElements(List<LocalizationUI> elements ) {
        translatableElements = elements;  
    }

}
