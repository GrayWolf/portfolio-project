﻿//public enum Languages { ENG = 0, ITA = 1, RUS = 2, JAP = 3 }

[System.Serializable]
public class LocalizationData
{ 
    public UnityEngine.SystemLanguage language;
    public LocalizationItem[] items;
}

[System.Serializable]
public class LocalizationItem
{
    public string key;
    public string value;
}