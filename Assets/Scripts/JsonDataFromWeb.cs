﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;

public class JsonDataFromWeb : MonoBehaviour
{
    public TMPro.TMP_Text txt;
    [SerializeField] string uri = "https://world.hui.land/cron/rand.php";
    // Start is called before the first frame update
    WaitForSeconds oneSecond = new WaitForSeconds(1.0f);
    public void Start()
    {
        StartCoroutine(ContactServer());
    }


    IEnumerator ContactServer()
    {
        while (true)
        {
            IEnumerator e =  GetValues();
            while (e.MoveNext())
            {
                yield return e.Current;
            }
            
            JsonData data = e.Current as JsonData;
            if(data != null)
            {
                Debug.Log($"Received: {data.code} {data.message}");
                txt.text = $"Received: CODE = {data.code}; MESS = {data.message}";
            }
            yield return oneSecond;
        }
    }

    IEnumerator GetValues()
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            if (webRequest.result == UnityWebRequest.Result.ConnectionError)
            {
                Debug.Log(pages[page] + ": Error: " + webRequest.error);
                yield return null;
            }
            else
            {
                JsonData data = JsonUtility.FromJson<JsonData>(webRequest.downloadHandler.text);
                yield return data;
            }
        }
    }
}
