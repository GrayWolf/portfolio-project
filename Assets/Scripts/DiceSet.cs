﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "DeD 5e/Dices")]
public class DiceSet : ScriptableObject
{
    public enum DiceType {None=0, D4 = 4, D6 = 6, D8 = 8, D10 = 10, D12 = 12, D20 = 20 }

	[SerializeField] private DiceType dice;
	[SerializeField] private int number;

	public int RollDice()
	{
		int result = 0;
		for (int i = 0; i < number; i++)
        {
			result += Random.Range(1, ((int)dice) + 1);
		}
		return result;
	}
	static public int Roll20()
    {
		return Random.Range(1, 21);
	}

	public int GetDiceType()
    {
		return (int)dice;
    }
}
