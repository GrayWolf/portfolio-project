﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;


public enum GameState {Boot, MainMenu,Loading,Running,Pause}
public delegate void EventGameState(GameState oldState, GameState currentState);

public class GameManager : Singleton<GameManager>
{
    [SerializeField] private GameState currentGameState = GameState.Boot;
    [SerializeField] private SystemLanguage currentLanguageUI = SystemLanguage.Unknown;
    
    public event EventGameState eventGameState;

    public GameState CurrentGameState
    {
        get
        {
            return currentGameState;
        }
    }

    public SystemLanguage CurrentLanguageUI
    {
        get
        {
            return currentLanguageUI;
        }
    }

    public void SetCurrentLanguageUI(SystemLanguage  newLanguage)
    {
        currentLanguageUI = newLanguage;
    }

    private void Awake()
    {
        currentLanguageUI = Application.systemLanguage;
        //TODO: Set currentLanguageUI = lastLanguageUI saved
        //My convention use the 0 for the boot/setup scene
        StartCoroutine(LoadLevel(1, LoadSceneMode.Additive,GameState.MainMenu));
    }

    void UpdateGameState(GameState newState)
    {
        GameState oldState = currentGameState;
        currentGameState = newState;
        if(eventGameState != null)
        {
            eventGameState(oldState,currentGameState);
        }
    }

    public IEnumerator LoadLevel(int id, LoadSceneMode mode, GameState? newState=null)
    {
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(id, mode);
        
        if (asyncOperation != null)
        {
            asyncOperation.completed += OnLoadCompleted;
        }
        while (!asyncOperation.isDone)
        {
            yield return null;
        }
        if(newState!=null)
            UpdateGameState((GameState)newState);
    }

    private void OnLoadCompleted(AsyncOperation obj)
    {
        Debug.Log("Load Completed");
    }

    public void QuitGame()
    {
        // save any game data here
#if UNITY_EDITOR
        // Application.Quit() does not work in the editor so
        // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
        UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_WEBGL
        Application.OpenURL("https://antoninopolizzi.it" );
#else
        Application.Quit();
#endif
    }


    public IEnumerator GetRequest(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            Debug.Log(uri);
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();
            Debug.Log(webRequest.downloadHandler.text);
            if (webRequest.isNetworkError)
            {
                Debug.Log("Error: " + webRequest.error);
                yield return null;
            }
            else
            {
                Debug.Log("Received: " + webRequest.downloadHandler.text);
                yield return webRequest.downloadHandler.text;
            }
            
        }
    }
}
