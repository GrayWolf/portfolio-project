﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LocalizedUIElements : MonoBehaviour
{
    [SerializeField]
    private LocalizationUI [] translatableElements;


    private void OnEnable()
    {
        if (translatableElements.Length > 0)
        {
            foreach (var element in translatableElements)
            {
                element.SetKey();
            }
            LocalizationManager.Instance.SetTranslatableUIElements(translatableElements.ToList());
            LocalizationManager.Instance.LocalizeUI(GameManager.Instance.CurrentLanguageUI);
        }
    }
}
