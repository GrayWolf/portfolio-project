using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ButtonClick : MonoBehaviour
{
    #region ScriptableRegion
    [SerializeField] DiceSet[] dices;
    [SerializeField] TMPro.TMP_Text txt;
    #endregion


    #region Localization Parameter
    private Dictionary<int, string> availableLanguages;
    private int min;
    private int max;
    public int value;
    #endregion

    private void Awake()
    {
        availableLanguages = LocalizationManager.Instance.GetAvailableLanguage();
        max = availableLanguages.Count - 1;
        min = 0;
        value = availableLanguages.Keys.ToList().IndexOf((int)GameManager.Instance.CurrentLanguageUI);
    }
    // Start is called before the first frame update
    public void Click()
    {
        if(dices.Length>0)
        {
            int index = Random.Range(0, dices.Length);
            txt.text = $"{dices[index].name} {dices[index].RollDice()}";
        }
        value = (value+1)  % (max+1);   
        LocalizationManager.Instance.LocalizeUI((SystemLanguage)availableLanguages.Keys.ElementAt(value));
    }
}
